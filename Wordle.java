import java.util.Scanner;
import java.util.Random;

/*
* Assignment 3 2023 - 110
* 
* Program  to play the game 
* wordle with user-inputted 
* guesses
* @author PKR.
* @version 2023-12
*/
public class Wordle {
	/*
	* Method that returns a randomly chosen word in uppercase.
	* Uses the Random class to choose the word, and the toUpperCase method to convert 
	* the word uppercase before rturning it.
	*/
	public static String generateWord(){
		Random randGen = new Random();
		String[] array = {"amigo", "adept", "among", "squad", "sport", "scary", 
		"wimpy", "wreck", "Wings", "witch", "awoke", "awake", "woken", "whorl", 
		"whirl", "noise", "voice", "vocal", "abode", "chase"};
		int index = randGen.nextInt(array.length);
		String answer = toUpperCase(array[index]);
		return answer;
	}
	/*
	* Method that checks if a given letter is in a given word.
	* Returns true if it is, and false if it isn't.
	* Parameters are the word (str), and the letter (char)
	*/
    public static boolean letterInWord(String word, char letter){
		for(int i = 0; i < word.length(); i++){
			if( letter == word.charAt(i)){
				return true;
			}
		}
		return false;
	}
	/*
	* Method checks if a letter is in a specific position in the answer word.
	* Returns true if it is and false if it isn't.
	* Parameters are the word (str), the letter (char), and the position (int)
	*/
	public static boolean letterInSlot(String word, char letter, int position){
		if(word.charAt(position) == letter){
			return true;
		}
		return false;
	}
	/*
	* Method that returns an array of colors representing correct, semi-correct, and
	* incorrect guesses.
	* Parameters are the answer word (str) and the user's guess (str).
	* If a letter in the guessed word is in the answer and in the same position of
	  the answer, the String in the array will be "green" (use letterInSlot for this)
	* If a letter is in the guessed word, but is in a different position, the String
	  in the array will be "yellow" (use letterInWord method for this)
	* Strings in the array are "white" by defult
	*/
	public static String[] guessWord(String answer, String guess){
		String[] colours = {"white","white","white","white","white"};
		
		for(int i = 0; i < guess.length(); i++){
			if(letterInWord(answer, guess.charAt(i))){
				colours[i] = "yellow";
				if(letterInSlot(answer, guess.charAt(i), i)){
					colours[i] = "green";
				}
			}
				
		}
		return colours;
	}
	/*
	* Method that displays the user's guessed word with colored letters. 
	* White means that the letter is not in the word, yellow means that the letter
	  is in the word but it isn't in the right position, and green means that the 
	  letter is in the correct position.
	* Parameters are the users guess (str), and the colours array.
	* Uses a For loop to check each letter.
	*/
	public static void presentResults(String guess, String[] colours){
		
		for(int i = 0; i < guess.length(); i++){
			if(colours[i].equals("green")){
				System.out.print("\u001B[32m" + guess.charAt(i) + "\u001B[0m ");
			} else if(colours[i].equals("yellow")){
				System.out.print("\u001B[33m" + guess.charAt(i) + "\u001B[0m ");
			} else {
				System.out.print(guess.charAt(i) + " ");
			}
		}
		System.out.println();
	}
	/*
	* Method that takes a char (c) as input, and returns the uppercase of 
	  that char regardless of whether that char is already uppercase or not.
	*/
	public static char charToUpperCase(char c) {
		if (c >= 'a' && c <= 'z') { 
			return c -= 32;
		}
		else {
			return c;
		}
	}
	/*
	*Method that iterates through every character in a given String, and converts it to
	 uppercase using the charToUpperCase Method.
	* It then returns that word in uppercase.
	*/
	public static String toUpperCase(String word){
		String uppercaseWord = "";
		for(int i = 0; i < word.length(); i++){
			uppercaseWord += charToUpperCase(word.charAt(i));
		}
		return uppercaseWord;
	}
	/*
	* Method that asks the user to guess a 5-letter word with no repeating characters.
	* If the word guessed is not 5 letters, it will ask again. This is done using
	  while loops.
	* The word will then be converted to uppercase using the toUpperCase method.
	* That uppercase word it then returned;
	*/
	public static String readGuess(){
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		System.out.println("please enter a 5 letter word with no repeating characters:");
		String guess = reader.next();
		
		while(guess.length() != 5){
			System.out.println("Your word is not the correct number of letters!");
			System.out.println("Please try again:");
			guess = reader.next();
		}
		guess = toUpperCase(guess);
		return guess;
	}
	/*
	* Method that Runs the Wordle game. 
	* Takes the answer word as a parameter.
	* Uses a while loop to prompt the user for guesses until they win or run out of
	  attempts.
	* Displays the results and updates counters after each guess.
	* Outputs a win or lose message at the end.
	*/
	public static void runGame(String answer){
		int numLettersGuessed = 0;
		int numOfAttempts = 6;
		int lengthOfAnswer = answer.length();
		while((numLettersGuessed != lengthOfAnswer) && (numOfAttempts != 0)){
			String guess = readGuess();
			String[] colours = guessWord(answer, guess);
			presentResults(guess, colours);
			numLettersGuessed = 0;
			for(int i = 0; i < lengthOfAnswer; i++){
				if(colours[i].equals("green")){
					numLettersGuessed++;
				}
			}
			numOfAttempts--;
		}
		if(numLettersGuessed == lengthOfAnswer){
			System.out.println("you win!");
		} else {
			System.out.println("you lose!");
		}
	}
}