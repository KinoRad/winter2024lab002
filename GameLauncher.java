import java.util.Scanner;
public class GameLauncher{
	public static void main(String[] args) {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		System.out.println("press 1 for Hangman, press 2 for wordle");
		int game = reader.nextInt();
		if(game == 1){
			System.out.println("You will be allowed 6 wrong guesses, after that you lose :)");
			System.out.println("Please enter a 4 letter word with no repeating characters:");
			String word = reader.next();
		
		HangMan.runGame(word);
		} else if(game == 2){
			String answer = Wordle.generateWord();
			Wordle.runGame(answer);
		}else{
			System.out.println("something went wrong");
		}
	}
}